extends CanvasLayer

var red_progressbar = load("res://dive/red_progressbar.tres")

func _ready():
	if GameState.is_diving:
		$Panel.visible = true
		$Dock.visible = false
	else:
		$Panel.visible = false
		$Dock.visible = true

func timeout():
	if GameState.is_diving:
		$Panel/OxygenLevel.value = 100.0 * GameState.oxygen / GameState.oxygen_max
		if GameState.oxygen < 10:
			$Panel/OxygenLevel.set("custom_styles/fg", red_progressbar)
			
		$Panel/DepthLabel.text = "%.1f fathom" % stepify(GameState.depth / 100, 0.1)
		$Panel/TreasureLabel.text = "%d treasures" % GameState.treasure_in_dive
	else:
		$Dock/BudgetLabel.text = "Budget: " + str(GameState.treasure)
