extends Timer

func timeout():
	var bubble_index = 1 + randi() % 3
	var bubble = get_parent().get_node("Bubble%d" % bubble_index)
	var bubble_sound = bubble.get_node("Sound") as AudioStreamPlayer2D
	bubble_sound.play()
