extends PathFollow2D

export var runSpeed = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(delta):
	set_offset(get_offset () + runSpeed*delta)
	
	if get_offset() > 1500:
		#$Sprite.flip_h = true
		$Sprite.flip_v = true
		
	if get_offset() < 100:
		#$Sprite.flip_h = false
		$Sprite.flip_v = false
