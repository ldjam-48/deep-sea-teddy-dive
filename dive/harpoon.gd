extends Area2D

var speed = 750

func _ready():
	set_as_toplevel(true)

func _physics_process(delta):
	position += transform.x * speed * delta



func _on_area_entered(area):
	if area.has_method("collect"):
		area.collect(self)
	self.queue_free()

func _on_body_entered(body):
	if body.get('is_submarine'):
		return
	
	# Wall
	visible = false
	$WallSound.connect("finished", self, "queue_free")
	$WallSound.play()
