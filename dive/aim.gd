extends Node2D

var harpoon_scene = preload("res://dive/harpoon.tscn")
export var delay = 2.0
var harpoon_ready = false

func _ready():
	if GameState.harpoon_upgrades > 0:
		harpoon_ready = true
		$Harpoon.visible = true
		$harpune_arrow.visible = true
	$Timer.wait_time = delay

func _process(delta):
	look_at(get_global_mouse_position())
	
func _input(event):
	if Input.is_action_just_pressed("shoot") && harpoon_ready:
		shoot()

func shoot():
	var harpoon = harpoon_scene.instance()
	add_child(harpoon)
	harpoon.transform = $Harpoon/Muzzle.global_transform
	harpoon_ready = false
	$harpune_arrow.visible = false
	$Timer.start()

func _on_Timer_timeout():
	harpoon_ready = true
	$harpune_arrow.visible = true
