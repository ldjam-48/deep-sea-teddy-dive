extends Node2D

# The buffer to spawn treasure below the submarine so we don't spawn stuff when it is already in view
const depth_buffer = 800 
const max_item_radius = 100
const step_size = 50
# The treasure_chance defines if an treasure should spawn at all. The relative chance of the treasure will then define which treasure will spawn
const treasure_chance = 150  #15%
# Same as treasure_chance but for enemies
const enemy_chance = 50  #5%

# Treasures which are spawnable at the current depth
var spawnable_treasures = []
# Treasures which will be spawnable at a lower depth
var pending_treasures = []
# The sum of the relative chances of the spawnable treasures
var total_treasure_chance = 0

# Treasures which are spawnable at the current depth
var spawnable_enemies = []
# Treasures which will be spawnable at a lower depth
var pending_enemies = []
# The sum of the relative chances of the spawnable treasures
var total_enemy_chance = 0

# Store the last items that have been spawned and that still have a chance to collide with items spawned in the future
var last_items = []
var last_calculated_depth = depth_buffer


func _ready():
	randomize()
	$SpawnArea.visible = false
	var available_items = load("res://items/items.tscn").instance() as Node
	
	for i in available_items.get_children():
		if i.is_obstacle:
			pending_enemies.append(i)
		else:
			pending_treasures.append(i)
	
func _process(delta):
	var spawning_area = GameState.depth + depth_buffer - last_calculated_depth
	var spawning_steps = floor(spawning_area / step_size)

	for i in spawning_steps:
		try_spawning_item()
		
func try_spawning_item():
	last_calculated_depth += step_size
	update_spawnable_items()
	
	var new_item: Node2D
	
	# Determins if a treasure, an enemy or nothing will be spawned
	var item_type_score = rand_range(0, 1000)
	
	# Use a random value to check wether to spawn an item
	if (item_type_score <= treasure_chance):
		new_item = spawn_random_treasure()
	elif (item_type_score <= treasure_chance + enemy_chance):
		new_item = spawn_random_enemy()
	
	if new_item != null:
		# Find a position for the new item
		new_item.position.x = rand_range($SpawnArea.margin_left, $SpawnArea.margin_right)
		new_item.position.y = last_calculated_depth
		
		var trys = 0
		var has_legal_position = check_for_collisions(new_item)
		while trys < 5 && !has_legal_position:
			new_item.position.x = rand_range($SpawnArea.margin_left, $SpawnArea.margin_right/2)
			has_legal_position = check_for_collisions(new_item)
			trys += 1
		
		# Only add the item if we found a legal position, otherwise, just discard it
		if has_legal_position:
			add_child(new_item)
			last_items.append(new_item)

func spawn_random_treasure() -> Node2D:
	# Use a random value to check which item to spawn
	var target_item_score = rand_range(0, total_treasure_chance)
	var current_item_score = 0 

	for item in spawnable_treasures:
		current_item_score += item.relative_chance

		if target_item_score <= current_item_score:
			return item.duplicate()
			
	return null
	
func spawn_random_enemy() -> Node2D:
	# Use a random value to check which item to spawn
	var target_item_score = rand_range(0, total_enemy_chance)
	var current_item_score = 0 

	for item in spawnable_enemies:
		current_item_score += item.relative_chance

		if target_item_score <= current_item_score:
			return item.duplicate()
			
	return null

 # Checks if an items collides with another items from the list of recently spawned items
func check_for_collisions(item: Node2D) -> bool:
	
	# Check for collisions with level architecture
	var space = get_world_2d().get_direct_space_state()
	var results = space.intersect_point(item.transform.origin, 32, [], 1024)
	if !results.empty():
		return false
	
	# Check for collisions with previously spawned items
	for i in last_items:
		var previous_item = i as Node2D
 
		# Check if the previous item can be safely removed from the list because it is too far away
		if last_calculated_depth - previous_item.position.y > max_item_radius * 2:
			last_items.erase(i)
		# Check if the previous item overlaps with the one to check
		elif	 previous_item.position.distance_to(item.position) < max_item_radius * 2:
			return false
			
	return true

# Moves items from the pending- to the spawnable list if the required minimal depth is reached
func update_spawnable_items():
	for treasure in pending_treasures:
		if treasure.min_depth <= last_calculated_depth:
			spawnable_treasures.append(treasure)
			pending_treasures.erase(treasure)
			total_treasure_chance += treasure.relative_chance
			
	for enemy in pending_enemies:
		if enemy.min_depth <= last_calculated_depth:
			spawnable_enemies.append(enemy )
			pending_enemies.erase(enemy )
			total_enemy_chance += enemy.relative_chance
