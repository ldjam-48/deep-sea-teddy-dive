extends Node2D


export var translation_speed = 5;
var direction = -1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if direction == 1:
		position.x = position.x + translation_speed		
		if position.x > 1500:
			direction = -1
			#$Sprite.flip_h = false
	
	elif direction == -1 :
		position.x = position.x - translation_speed
	
		if position.x < 0:
			direction = 1
			#$Sprite.flip_h = true
