extends CanvasLayer

func toggled(button_pressed):
	AudioServer.set_bus_mute(0, not button_pressed)

func focus_entered():
	$Button.release_focus()
