extends CanvasLayer

func _ready():
	hide()

func hide():
	for c in get_children():
		c.visible = false

func show():
	for c in get_children():
		c.visible = true
