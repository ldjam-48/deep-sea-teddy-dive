extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	GameState.connect("upgrade_purchased", self, "check_upgrades")
	check_upgrades()

func check_upgrades():
	check_oxygen_upgrades()
	check_drive_upgrades()
	check_light_upgrades()
	
func check_oxygen_upgrades():
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == GameState.UPGRADE_TYPES.oxygen_tanks && upgrade["purchased"]:
			get_node("oxygen").visible = true
			break
	
func check_drive_upgrades():
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == GameState.UPGRADE_TYPES.drive && upgrade["purchased"]:
			get_node("drive").visible = true
			break
			
func check_light_upgrades():
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == GameState.UPGRADE_TYPES.light && upgrade["purchased"]:
			get_node("light").visible = true
			break

