extends Area2D

export var upgrade_type = GameState.UPGRADE_TYPES.oxygen_tanks
var collected = false

func _ready():
	if is_collected():
		queue_free()
		return
		
	var value = name	
		
	for sprite in $Sprites.get_children():
		sprite.visible = (name == "Recipe_" + sprite.name)	
		

func body_entered(body):
	if body.get('is_submarine'):
		collect(body)

func collect(body):
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == upgrade_type:
			upgrade["found"] = true
			$Sound.connect("finished", self, "queue_free")
			$Sound.play()

func is_collected():
	var collected = false
	for upgrade in GameState.UPGRADES:
		if (upgrade["type"] == upgrade_type && upgrade["found"] == true):
			collected = true
			break
	return collected
