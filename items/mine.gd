extends Area2D

export var value = 1
export var min_depth = 0
export var relative_chance = 100
export var is_obstacle = true

var was_hit = false

func _ready():
	var scale = value / 10.0
	$CollisionShape2D.scale = Vector2(scale, scale)
	$Sprite.scale = Vector2(scale, scale)

func body_entered(body: Node):
	if body.get('is_submarine'):
		collect(body)

func collect(body):
	if was_hit:
		return
		
	if body.get('is_submarine'):
		GameState.oxygen = max(GameState.oxygen - value, 0)
		if body.has_method("hit_mine"):
			body.hit_mine()
			
		var modal = load("res://modal/mine_modal.tscn").instance()
		get_tree().root.add_child(modal)

	was_hit = true
	$Sprite.visible = false
	$Particles2D.emitting = true
	$AudioStreamPlayer.connect("finished", $Timer, "start")
	$AudioStreamPlayer.play()
	get_parent().last_items.erase(self)




func timeout():
	queue_free()
