extends Area2D

export var value = 1
export var min_depth = 0
export var relative_chance = 100
export var is_obstacle = false

func _ready():
	var sprite_index = randi()%$Sprites.get_child_count()
	for i in range($Sprites.get_child_count()):
		$Sprites.get_child(i).visible = (i == sprite_index)

func body_entered(body):
	if body.get('is_submarine'):
		collect(body)
	
func collect(body):
	GameState.treasure_in_dive += value
	
	var modal = load("res://modal/treasure_modal.tscn").instance()
	get_tree().root.add_child(modal)
		
	visible = false
	$AudioStreamPlayer.connect("finished", self, "queue_free")
	$AudioStreamPlayer.play()
	get_parent().last_items.erase(self)
