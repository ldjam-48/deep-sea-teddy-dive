extends CanvasLayer

var text = [
	"""Kim lives with their parents on a platform in the ocean.

They often play fetch or hide or just look into the deep water.""",

	"""One day Kim walks really close to the edge of the platform to get a glimpse of what’s underneath.""",

	"""Of course Kim’s favorite Teddy bear joins this adventure.

But just as Kim wants to return to their parents, Teddy slips out of the hand and into the water.""",

	"""This is the worst that ever could happen to Kim.

Screaming and sobbing doesn’t seem to help, so after some time Kim asks their parents for help.""",

	"""One of Kim’s parents immediately enters the submarine expedition vessel and starts searching.

Going deeper and deeper, new surprises and dangers await."""

]

func _ready():
	continue_story()

func continue_story():
	if text.empty():
		finish_story()
		return

	$RichTextLabel.bbcode_text = text.pop_front()
	if text.empty():
		$EnoughButton.hide()
		$ContinueButton.text = "Let's go"

func finish_story():
	queue_free()
