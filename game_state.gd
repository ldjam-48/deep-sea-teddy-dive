extends Node

var oxygen_max = 75
var oxygen = 75
var depth = 0
var treasure = 0 setget set_treasure
var treasure_in_dive = 0
var oxygen_upgrades = 0
var movement_upgrades = 0
var diving_upgrades = 0
var harpoon_upgrades = 0
var light_upgrades = 0
var is_diving = false
signal treasure_changed
signal upgrade_purchased

enum UPGRADE_TYPES {
	oxygen_tanks,
	communication,
	science,
	drive,
	harpoon,
	light
}
	

var UPGRADES = [
	#Hull
	{"name": "science", "found": false, "purchased": false, "type": UPGRADE_TYPES.science, "description": "Build a science lab to find new ways of compressing oxygen more efficiently.", "cost": 250},
	{"name": "comm", "found": false, "purchased": false, "type": UPGRADE_TYPES.communication, "description": "Build a communications array to improve navigation during the dive.", "cost": 250},
	{"name": "drive", "found": false, "purchased": false, "type": UPGRADE_TYPES.drive, "description": "Build a drive for your submarine to improve maneuverability.", "cost": 100},
	{"name": "oxygen", "found": false, "purchased": false, "type": UPGRADE_TYPES.oxygen_tanks, "description": "Add extra oxygen tanks to your submarine for longer diving sessions.", "cost": 100},
	{"name": "harpoon", "found": false, "purchased": false, "type": UPGRADE_TYPES.harpoon, "description": "Collect treasure from a distance with this handy harpoon.", "cost": 100},
	{"name": "light", "found": false, "purchased": false, "type": UPGRADE_TYPES.light, "description": "3 AAA batteries included for free (works with AA batteries)", "cost": 150}
	#Gadgets
	#{"name": "Harpoon", "found": true, "purchased": false, "type": UPGRADE_TYPES.harpoon, "description": "Can be used to grab materials from far away", "cost": 300},
	#Steering upgrades
	#{"name": "Propeller", "found": true, "purchased": false, "type": UPGRADE_TYPES.movement_modifier, "description": "Used to improve manouverability", "cost": 200},
	#{"name": "Mittelgroßer Propeller", "found": true, "purchased": false, "type": UPGRADE_TYPES.movement_modifier, "description": "Used to improve manouverability", "cost": 300},
	#{"name": "Propeller, der sehr gut ist", "found": true, "purchased": false, "type": UPGRADE_TYPES.movement_modifier, "description": "Used to improve manouverability", "cost": 500},
	#{"name": "Bigger Oxygen Tank ", "found": true, "purchased": false, "type": UPGRADE_TYPES.oxygen_capacity, "description": "Used to improve oxygen capacity, duh.", "cost": 200},
	#{"name": "Oxygen Tank Efficiency Upgrade", "found": true, "purchased": false, "type": UPGRADE_TYPES.oxygen_capacity, "description": "Oxygen is defragmented before storage to save space", "cost": 500}
]

func set_treasure(value):
	treasure = value
	emit_signal("treasure_changed", value)
	
func upgrade(type):
	match [type]:
		[UPGRADE_TYPES.oxygen_tanks]:
			oxygen_upgrades += 1
		[UPGRADE_TYPES.drive]:
			#diving_upgrades += 1
			movement_upgrades += 1
		[UPGRADE_TYPES.communication]:
			movement_upgrades += 1
		[UPGRADE_TYPES.science]:
			oxygen_upgrades += 1
		[UPGRADE_TYPES.harpoon]:
			harpoon_upgrades += 1
		[UPGRADE_TYPES.light]:
			light_upgrades += 1
	emit_signal("upgrade_purchased")

var shown_modals = []
