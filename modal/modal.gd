extends CanvasLayer

func _ready():
	if GameState.shown_modals.has(name):
		close()
		return

	get_tree().paused = true
	GameState.shown_modals.append(name)
	scale = Vector2(1, 1)

func close():
	queue_free()
	get_tree().paused = false
