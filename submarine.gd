extends RigidBody2D

enum HORIZONTAL_DIRECTION {
	none,
	left,
	right
}

const initial_oxygen = 75

var movement_per_second = 100
var decelerate_force = 500
var diving_gravity = 1
var max_sinking_speed = 300.0


const resurfacing_gravity = -5
const turnaround_force = 200

var initial_transform = Transform2D()
var current_direction = HORIZONTAL_DIRECTION.none
var is_decelerating = false
var is_resurfacing = false
var is_moving = false

export var is_submarine = true

func _ready():
	initial_transform = transform
	movement_per_second += GameState.movement_upgrades * 75
	max_sinking_speed += GameState.diving_upgrades * 100
	GameState.oxygen_max = initial_oxygen + GameState.oxygen_upgrades * 50
	GameState.oxygen = GameState.oxygen_max
	$Light2D.enabled = (GameState.light_upgrades != 0)
	linear_velocity.y = 750

	var modal = load("res://modal/keybindings_modal.tscn").instance()
	get_tree().root.add_child(modal)

func _input(event):
	# Left
	if event.is_action_released("ui_left") && current_direction == HORIZONTAL_DIRECTION.left:
		current_direction = HORIZONTAL_DIRECTION.none
	elif event.is_action("ui_left") && current_direction == HORIZONTAL_DIRECTION.none:
		current_direction = HORIZONTAL_DIRECTION.left
		
	# Right
	if event.is_action_released("ui_right") && current_direction == HORIZONTAL_DIRECTION.right:
		current_direction = HORIZONTAL_DIRECTION.none
	elif event.is_action("ui_right") && current_direction == HORIZONTAL_DIRECTION.none:
		current_direction = HORIZONTAL_DIRECTION.right
	
	# Decelerate	
	if event.is_action_pressed("ui_up"):
		is_decelerating = true
	elif event.is_action_released("ui_up"):
		is_decelerating = false
		
	# Start Resurfacing
	if event.is_action_released("ui_select"):
		start_resurfacing()


func _physics_process(delta):
	# Depth
	GameState.depth = position.y - initial_transform.origin.y
	if GameState.depth < 0:
		reset()

	# Max sinking speed
	if GameState.depth > 500 and linear_velocity.y > max_sinking_speed:
		linear_velocity.y = max_sinking_speed
	
	# Horizontal movement
	match [current_direction]:
		[HORIZONTAL_DIRECTION.left]:
			linear_velocity.x -= movement_per_second * delta
		[HORIZONTAL_DIRECTION.right]:
			linear_velocity.x += movement_per_second * delta
	
	# Decelerating
	if is_decelerating && !is_resurfacing:
		linear_velocity.y = max (linear_velocity.y - decelerate_force * delta, 0)

func reset():
	GameState.is_diving = false
	get_tree().change_scene("res://dock/dock.tscn")

func start_resurfacing():
	if is_resurfacing:
		return
		
	is_resurfacing = true
	linear_velocity.y -= turnaround_force
	gravity_scale = resurfacing_gravity

func hit_mine():
	if !is_resurfacing:
		linear_velocity.y = 1.0

func timeout():
	var oxygen_loss = 1

	match [current_direction]:
		[HORIZONTAL_DIRECTION.left, HORIZONTAL_DIRECTION.right]:
			oxygen_loss += 0.5

	if is_decelerating && !is_resurfacing:
		oxygen_loss += 0.7

	GameState.oxygen = max(GameState.oxygen - oxygen_loss, 0)
	if GameState.oxygen == 0:
		var modal = load("res://modal/no_oxygen_modal.tscn").instance()
		get_tree().root.add_child(modal)

		start_resurfacing()
