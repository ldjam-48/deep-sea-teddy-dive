extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	GameState.connect("upgrade_purchased", self, "check_upgrades")
	check_upgrades()

func check_upgrades():
	check_science_upgrades()
	check_comm_upgrades()
	check_harpoon_upgrades()
	
func check_science_upgrades():
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == GameState.UPGRADE_TYPES.science && upgrade["purchased"]:
			get_node("Science").visible = true
			break
	
func check_comm_upgrades():
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == GameState.UPGRADE_TYPES.communication && upgrade["purchased"]:
			get_node("Comm").visible = true
			break

func check_harpoon_upgrades():
	for upgrade in GameState.UPGRADES:
		if upgrade["type"] == GameState.UPGRADE_TYPES.harpoon && upgrade["purchased"]:
			get_node("Harpoon").visible = true
			break
