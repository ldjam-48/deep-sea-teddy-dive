extends Control

var thread

func _ready():
	GameState.treasure += GameState.treasure_in_dive
	GameState.treasure_in_dive = 0

func dive():
	GameState.is_diving = true
	LoadingScreen.show()
	# wait for rendering
	yield(get_tree().create_timer(0.5), "timeout")
	get_tree().change_scene("res://dive/dive.tscn")
	LoadingScreen.hide()
