extends GridContainer

var selected_upgrade = null;

# Called when the node enters the scene tree for the first time.
func _ready():
	var shop_item_scene = load("res://dock/shop_item.tscn")
	for i in range(0, GameState.UPGRADES.size()):
	#  upgrade in GameState.UPGRADES:
		var shop_item = shop_item_scene.instance() as TextureButton
		shop_item.upgrade = GameState.UPGRADES[i]
		shop_item.index = i
		if not shop_item.upgrade["purchased"] and shop_item.upgrade["found"]:
			add_child(shop_item)
