extends Node2D

export var translation_speed = 1.0
var direction = 1
#export var translation_speed = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	if direction == 1:
		$AnimatedSprite.flip_h = true
	elif direction == -1:
		$AnimatedSprite.flip_h = false
	$AnimatedSprite.play("default")

func _process(delta):
	if direction == 1:
		position.x = position.x + translation_speed		
		if position.x > 600:
			direction = -1
			$AnimatedSprite.flip_h = false
	elif direction == -1 :
		position.x = position.x - translation_speed
		if position.x < 100:
			direction = 1
			$AnimatedSprite.flip_h = true
