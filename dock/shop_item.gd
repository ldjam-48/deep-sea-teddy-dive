extends TextureButton

export var upgrade = {"name": "", "found": false, "purchased": false, "type": null, "description": "", "cost": 0}
var index
# Called when the node enters the scene tree for the first time.
func _ready():
	# self.text = upgrade["name"]+"("+str(upgrade["cost"])+")"
	$Label.text = upgrade["name"]

	self.texture_normal = load("res://dock/button_" + upgrade["name"] + ".png")
	self.texture_disabled = load("res://dock/button_" + upgrade["name"] + "_disabled.png")
	self.texture_hover = load("res://dock/button_" + upgrade["name"] + "_hover.png")

	self.disabled = not can_afford(GameState.treasure) || upgrade["purchased"]
	# $Label.text = upgrade["description"]
	GameState.connect("treasure_changed", self , "update_status")
	self.hint_tooltip = "cost: "+str(upgrade["cost"])+" \n\n" + upgrade["description"]

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_button_up():
	GameState.treasure -= upgrade["cost"]
	GameState.UPGRADES[index].purchased = true
	GameState.upgrade(upgrade["type"])
	visible = false
	$AudioStreamPlayer.play()

func can_afford(treasure_value) -> bool:
	if upgrade["cost"] > treasure_value:
		return false
	else:
		return true
		
func update_status(treasure_value):
	self.disabled = not can_afford(treasure_value) || upgrade["purchased"]


func _on_mouse_entered():
	$Label.visible = true
	
func _on_mouse_exit():
	$Label.visible = false
